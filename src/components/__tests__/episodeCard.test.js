import "react-native";
import React from "react";
import { EpisodeCard } from "../episodeCard";

import { render } from "react-native-testing-library";

describe("Testing Episode Card", () => {
  test("Testing proper Rendering", async () => {
    let mockObj = {
      name: "Pilot",
      air_date: "11-02-2020",
      episode: "S1-11",
      created: "11-01-2020",
    };
    let component = <EpisodeCard data={mockObj} />;
    const { findByTestId, findAllByText, toJSON } = render(component);
    let nameText = await findByTestId("name");
    let nameHeading = await findAllByText("Episode Name:");
    expect(nameHeading).toBeTruthy();
    expect(nameText.props.children).toEqual(mockObj.name);
    let air_dateText = await findByTestId("air_date");
    let air_dateHeading = await findAllByText("Episode AirDate:");
    expect(air_dateHeading).toBeTruthy();
    expect(air_dateText.props.children).toEqual(mockObj.air_date);
    let episodeText = await findByTestId("episode");
    let episodeHeading = await findAllByText("Episode Code:");
    expect(episodeHeading).toBeTruthy();
    expect(episodeText.props.children).toEqual(mockObj.episode);
    let createdText = await findByTestId("created");
    let createdHeading = await findAllByText("Created On:");
    expect(createdHeading).toBeTruthy();
    expect(createdText.props.children).toEqual(mockObj.created);
    expect(toJSON()).toMatchSnapshot();
  });
});
