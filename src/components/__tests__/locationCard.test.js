import "react-native";
import React from "react";
import { render } from "react-native-testing-library";
import { LocationCard } from "../locationCard";

describe("Testing Location Card", () => {
  test("Testing proper Rendering", async () => {
    let mockObj = {
      name: "Earth",
      type: "Planet",
      dimension: "s123",
      created: "11-01-2020",
    };
    let component = <LocationCard data={mockObj} />;
    const { findByTestId, findAllByText, toJSON } = render(component);
    let nameText = await findByTestId("name");
    let nameHeading = await findAllByText("Location Name:");
    expect(nameHeading).toBeTruthy();
    expect(nameText.props.children).toEqual(mockObj.name);
    let typeText = await findByTestId("type");
    let typeHeading = await findAllByText("Location Type:");
    expect(typeHeading).toBeTruthy();
    expect(typeText.props.children).toEqual(mockObj.type);
    let dimensionText = await findByTestId("dimension");
    let dimensionHeading = await findAllByText("Location Dimension:");
    expect(dimensionHeading).toBeTruthy();
    expect(dimensionText.props.children).toEqual(mockObj.dimension);
    let createdText = await findByTestId("created");
    let createdHeading = await findAllByText("Created On:");
    expect(createdHeading).toBeTruthy();
    expect(createdText.props.children).toEqual(mockObj.created);
    expect(toJSON()).toMatchSnapshot();
  });
});
