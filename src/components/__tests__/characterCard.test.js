import "react-native";
import React from "react";
import { CharacterCard } from "../characterCard";

import { render } from "react-native-testing-library";

describe("Testing Episode Card", () => {
  test("Testing proper Rendering", async () => {
    let mockObj = {
      created: "2017-11-04T18:48:46.250Z",
      name: "Rick Morty",
      image: "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
      status: "alive",
      species: "Human",
      episode: [
        "https://rickandmortyapi.com/api/episode/1",
        "https://rickandmortyapi.com/api/episode/2",
      ],
      location: {
        name: "Earth (Replacement Dimension)",
        url: "https://rickandmortyapi.com/api/location/20",
      },

      origin: {
        name: "Earth (C-137)",
        url: "https://rickandmortyapi.com/api/location/1",
      },
    };
    let handleNavigation = jest.fn;
    let component = (
      <CharacterCard {...mockObj} handleNavigation={handleNavigation} />
    );
    const { findByTestId, toJSON } = render(component);
    let nameText = await findByTestId("name");
    expect(nameText.props.children).toEqual(mockObj.name);

    let locationText = await findByTestId("location");
    expect(locationText.props.children).toEqual(mockObj.location.name);
    let originText = await findByTestId("origin");
    expect(originText.props.children).toEqual(mockObj.origin.name);
    expect(toJSON()).toMatchSnapshot();
  });
});
