import React from "react";
import { Text, View, StyleSheet, Dimensions } from "react-native";
import { colors, fonts } from "../utils/theme";

const { width } = Dimensions.get("window");
export const LocationCard = ({ data }) => {
  const { name, type, dimension, created } = data;
  return (
    <View style={styles.container}>
      <View style={styles.columnContainer}>
        <Text style={styles.headings}>Location Name:</Text>
        <Text testID="name" style={styles.value}>
          {name}
        </Text>
        <Text style={styles.headings}>Location Type:</Text>
        <Text testID="type" style={styles.value}>
          {type}
        </Text>
        <Text style={styles.headings}>Location Dimension:</Text>
        <Text testID="dimension" style={styles.value}>
          {dimension}
        </Text>
        <Text style={styles.headings}>Created On:</Text>
        <Text testID="created" style={styles.value}>
          {created}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: width * 0.75,
    backgroundColor: colors.mainThemeColor,
    borderRadius: 8,
    margin: 10,
  },
  columnContainer: {
    display: "flex",
    flexDirection: "column",
    padding: 10,
  },
  headings: {
    fontSize: fonts.medium,
    paddingHorizontal: 5,
    color: colors.headingsColor,
  },
  value: {
    fontSize: fonts.medium,
    fontWeight: "500",
    color: colors.white,
  },
});
