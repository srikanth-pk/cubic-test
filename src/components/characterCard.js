import React from "react";
import { Text, View, StyleSheet, Image, Dimensions } from "react-native";
import { colors, fonts } from "../utils/theme";

const { width, height } = Dimensions.get("window");
export const CharacterCard = ({
  name,
  image,
  status,
  species,
  location,
  episode,
  origin,
  handleNavigation,
}) => {
  return (
    <View style={styles.container}>
      <Image
        testID="image"
        source={{ uri: image }}
        style={styles.characterImage}
      ></Image>
      <View style={styles.columnContainer}>
        <Text testID="name" style={styles.name}>
          {name}
        </Text>
        <Text style={styles.status}>
          <View
            style={[
              styles.circle,
              status.toLowerCase() == "unknown"
                ? styles.unknown
                : status.toLowerCase() == "dead"
                ? styles.dead
                : styles.alive,
            ]}
          ></View>
          {status} - {species}
        </Text>
        <Text style={styles.headings}>Last known location:</Text>

        <Text
          testID="location"
          onPress={() => {
            handleNavigation("Location", location.url, name);
          }}
          style={styles.value}
        >
          {location.name}
        </Text>

        <Text style={styles.headings}>First seen in:</Text>

        <Text
          testID="origin"
          onPress={() => handleNavigation("Episode", episode, name)}
          style={styles.value}
        >
          {origin.name}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: width * 0.75,
    backgroundColor: colors.mainThemeColor,
    borderRadius: 8,
    margin: 10,
  },
  columnContainer: {
    display: "flex",
    flexDirection: "column",
    padding: 10,
  },
  characterImage: {
    width: "100%",
    height: height * 0.25,
    flex: 1,
    resizeMode: "stretch",
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  name: {
    fontSize: fonts.Large,
    fontWeight: "bold",
    color: colors.white,
  },
  status: {
    display: "flex",
    fontSize: fonts.medium,
    fontWeight: "500",
    color: colors.white,
    alignItems: "center",
  },
  headings: {
    fontSize: fonts.medium,
    paddingTop: 5,
    paddingBottom: 5,
    color: colors.headingsColor,
  },
  value: {
    fontSize: 16,
    fontWeight: "500",
    color: colors.white,
  },
  circle: {
    height: 10,
    width: 10,
    borderRadius: 1000,
    marginRight: 5,
  },
  dead: {
    backgroundColor: colors.red,
  },
  alive: {
    backgroundColor: colors.green,
  },
  unknown: {
    backgroundColor: colors.grey,
  },
});
