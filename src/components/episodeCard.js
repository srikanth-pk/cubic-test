import React from "react";
import { Text, View, StyleSheet, Dimensions } from "react-native";
import { colors, fonts } from "../utils/theme";

const { width } = Dimensions.get("window");
export const EpisodeCard = ({ data }) => {
  const { name, air_date, episode, created } = data;
  return (
    <View style={styles.container}>
      <View style={styles.columnContainer}>
        <Text style={styles.headings}>Episode Name:</Text>
        <Text testID="name" style={styles.value}>
          {name}
        </Text>
        <Text style={styles.headings}>Episode AirDate:</Text>
        <Text testID="air_date" style={styles.value}>
          {air_date}
        </Text>
        <Text style={styles.headings}>Episode Code:</Text>
        <Text testID="episode" style={styles.value}>
          {episode}
        </Text>
        <Text style={styles.headings}>Created On:</Text>
        <Text testID="created" style={styles.value}>
          {created}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: width * 0.75,
    backgroundColor: colors.mainThemeColor,
    borderRadius: 8,
    margin: 10,
  },
  columnContainer: {
    display: "flex",
    flexDirection: "column",
    padding: 10,
  },
  headings: {
    fontSize: fonts.medium,
    paddingHorizontal: 5,
    color: colors.headingsColor,
  },
  value: {
    fontSize: fonts.medium,
    fontWeight: "500",
    color: colors.white,
  },
});
