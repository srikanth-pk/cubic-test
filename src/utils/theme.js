export const colors = {
  mainThemeColor: "rgb(60,62,68)",
  backgroundColor: "black",
  headingsColor: "rgb(158, 158, 158)",
  white: "#fff",
  red: "red",
  green: "green",
  grey: "grey",
};
export const fonts = {
  small: 12,
  medium: 16,
  Large: 18,
};
