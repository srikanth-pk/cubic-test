import React, { useEffect, useState } from "react";
import { LocationCard } from "../components/locationCard";
import {
  ActivityIndicator,
  Text,
  StyleSheet,
  SafeAreaView,
  View,
} from "react-native";
import { colors } from "../utils/theme";
export const LocationDetails = ({ route }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);

  let _isMounted = false;

  useEffect(() => {
    _isMounted = true;
    fetchRecords();
    return () => {
      _isMounted = false;
    };
  }, []);

  const fetchRecords = () => {
    if (_isMounted) {
      fetch(route.params.url)
        .then((response) => response.json())
        .then((data) => {
          {
            setData(data);
            setIsLoading(false);
          }
        })
        .catch((error) => {
          {
            setError(error);
            setIsLoading(false);
          }
        });
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      {isLoading ? (
        <View style={styles.container}>
          <ActivityIndicator size="large" color={colors.white} />
        </View>
      ) : error ? (
        <View style={styles.container}>
          <Text style={{ fontSize: 18 }}>
            Error fetching data... Check your network connection!
          </Text>
        </View>
      ) : (
        <LocationCard data={data} />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingVertical: 10,
    backgroundColor: colors.backgroundColor,
  },
});
