import "react-native";
import React from "react";
import { render, waitFor } from "react-native-testing-library";
import { EpisodeDetails } from "../episodeDetails";

describe("Testing EpisodePage", () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });
  test("EpisodePage  Rendering", async () => {
    fetchMock.mockResponse(
      JSON.stringify({
        id: 1,
        name: "Pilot",
        air_date: "11-02-2020",
        episode: "S1-11",
        created: "11-01-2020",
      })
    );
    let route = {
      params: {
        url: [
          "https://rickandmortyapi.com/episode/1",
          "https://rickandmortyapi.com/episode/3",
          "https://rickandmortyapi.com/episode/4",
        ],
      },
    };
    const component = <EpisodeDetails route={route} />;
    const { getByTestId, getAllByTestId, toJSON } = render(component);
    const listNode = await waitFor(() => getByTestId("episode-list"));
    expect(listNode.props.data).toHaveLength(3);
    const textName = await waitFor(() => getAllByTestId("name"));
    expect(textName.length).toBe(3);
    expect(textName[0].props.children).toBe("Pilot");
    expect(toJSON()).toMatchSnapshot();
  });
});
