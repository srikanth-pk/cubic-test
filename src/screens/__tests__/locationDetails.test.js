import "react-native";
import React from "react";
import { LocationDetails } from "../locationDetails";

import { render, waitFor } from "react-native-testing-library";

describe("Testing LocationPage", () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });
  test("LocationPage  Rendering", async () => {
    fetchMock.mockResponse(
      JSON.stringify({
        name: "Earth",
        type: "Planet",
        dimension: "s123",
        created: "11-01-2020",
      })
    );
    let route = {
      params: {
        url: "https://rickandmortyapi.com",
      },
    };
    const { getByTestId, toJSON } = render(<LocationDetails route={route} />);
    let nameText = await waitFor(() => getByTestId("name"));
    expect(nameText.props.children).toBe("Earth");
    expect(toJSON()).toMatchSnapshot();
  });
});
