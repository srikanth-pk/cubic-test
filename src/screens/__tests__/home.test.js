import "react-native";
import React from "react";
import { Home } from "../home";
import { fireEvent, render, waitFor } from "react-native-testing-library";

describe("Testing HomePage", () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });
  test("Home Page List Rendering", async () => {
    fetchMock.mockResponse(
      JSON.stringify({
        results: [
          {
            created: "2017-11-04T18:48:46.250Z",
            episode: [
              "https://rickandmortyapi.com/api/episode/1",
              "https://rickandmortyapi.com/api/episode/2",
            ],
            gender: "Male",
            id: 1,
            image: "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
            location: {
              name: "Earth (Replacement Dimension)",
              url: "https://rickandmortyapi.com/api/location/20",
            },
            name: "Rick Sanchez",
            origin: {
              name: "Earth (C-137)",
              url: "https://rickandmortyapi.com/api/location/1",
            },
            species: "Human",
            status: "Alive",
            type: "",
            url: "https://rickandmortyapi.com/api/character/1",
          },
          {
            created: "2017-11-04T18:48:46.250Z",
            episode: [
              "https://rickandmortyapi.com/api/episode/1",
              "https://rickandmortyapi.com/api/episode/2",
            ],
            gender: "Female",
            id: 2,
            image: "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
            location: {
              name: "Earth (Replacement Dimension)",
              url: "https://rickandmortyapi.com/api/location/20",
            },
            name: "Big Morty",
            origin: {
              name: "Earth (C-137)",
              url: "https://rickandmortyapi.com/api/location/1",
            },
            species: "Alien",
            status: "Dead",
            type: "",
            url: "https://rickandmortyapi.com/api/character/1",
          },
          {
            created: "2017-11-04T18:48:46.250Z",
            episode: [
              "https://rickandmortyapi.com/api/episode/1",
              "https://rickandmortyapi.com/api/episode/2",
            ],
            gender: "Male",
            id: 3,
            image: "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
            location: {
              name: "Earth (Replacement Dimension)",
              url: "https://rickandmortyapi.com/api/location/20",
            },
            name: "Rick Morty",
            origin: {
              name: "Earth (C-137)",
              url: "https://rickandmortyapi.com/api/location/1",
            },
            species: "Human",
            status: "alive",
            type: "",
            url: "https://rickandmortyapi.com/api/character/1",
          },
        ],
      })
    );

    const goBack = jest.fn();
    const navigate = jest.fn();
    const setOptions = jest.fn();

    const { getByTestId, toJSON } = render(
      <Home navigation={{ goBack, navigate, setOptions }} />
    );

    const listNode = await waitFor(() => getByTestId("list"));
    expect(listNode.props.data).toHaveLength(3);
  });
  test("Home Page Search Filtered List Rendering", async () => {
    fetchMock.mockResponse(
      JSON.stringify({
        results: [
          {
            created: "2017-11-04T18:48:46.250Z",
            episode: [
              "https://rickandmortyapi.com/api/episode/1",
              "https://rickandmortyapi.com/api/episode/2",
            ],
            gender: "Male",
            id: 1,
            image: "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
            location: {
              name: "Earth (Replacement Dimension)",
              url: "https://rickandmortyapi.com/api/location/20",
            },
            name: "Rick Sanchez",
            origin: {
              name: "Earth (C-137)",
              url: "https://rickandmortyapi.com/api/location/1",
            },
            species: "Human",
            status: "Alive",
            type: "",
            url: "https://rickandmortyapi.com/api/character/1",
          },
          {
            created: "2017-11-04T18:48:46.250Z",
            episode: [
              "https://rickandmortyapi.com/api/episode/1",
              "https://rickandmortyapi.com/api/episode/2",
            ],
            gender: "Female",
            id: 2,
            image: "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
            location: {
              name: "Earth (Replacement Dimension)",
              url: "https://rickandmortyapi.com/api/location/20",
            },
            name: "Big Morty",
            origin: {
              name: "Earth (C-137)",
              url: "https://rickandmortyapi.com/api/location/1",
            },
            species: "Alien",
            status: "Dead",
            type: "",
            url: "https://rickandmortyapi.com/api/character/1",
          },
          {
            created: "2017-11-04T18:48:46.250Z",
            episode: [
              "https://rickandmortyapi.com/api/episode/1",
              "https://rickandmortyapi.com/api/episode/2",
            ],
            gender: "Male",
            id: 3,
            image: "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
            location: {
              name: "Earth (Replacement Dimension)",
              url: "https://rickandmortyapi.com/api/location/20",
            },
            name: "Rick Morty",
            origin: {
              name: "Earth (C-137)",
              url: "https://rickandmortyapi.com/api/location/1",
            },
            species: "Human",
            status: "alive",
            type: "",
            url: "https://rickandmortyapi.com/api/character/1",
          },
        ],
      })
    );

    const goBack = jest.fn();
    const navigate = jest.fn();
    const setOptions = jest.fn();

    const { getByTestId, toJSON } = render(
      <Home navigation={{ goBack, navigate, setOptions }} />
    );
    const testInput = await waitFor(() => getByTestId("inputText"));
    fireEvent.changeText(testInput, "Morty");
    const listNode = await waitFor(() => getByTestId("list"));
    expect(listNode.props.data).toHaveLength(2);
    expect(toJSON()).toMatchSnapshot();
  });
});
