import React, { useState, useEffect } from "react";
import {
  ActivityIndicator,
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  SafeAreaView,
  View,
} from "react-native";
import { CharacterCard } from "../components/characterCard";
import { colors, fonts } from "../utils/theme";

export const Home = ({ navigation }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [query, setQuery] = useState("");
  const [fullData, setFullData] = useState([]);
  const [pageNo, setPageNo] = useState(0);
  const [isLoadingMore, setIsLoadingMore] = useState(false);

  let _isMounted = false;
  let url = "https://rickandmortyapi.com/api/character/?page=";
  useEffect(() => {
    _isMounted = true;
    fetchRecords(1);
    return () => {
      _isMounted = false;
    };
  }, []);

  const fetchRecords = (pageNo) => {
    if (_isMounted) {
      setPageNo(pageNo);
      fetch(url + pageNo)
        .then((response) => response.json())
        .then((json) => {
          {
            setData([...fullData, ...json.results]);
            setFullData([...fullData, ...json.results]);
            setIsLoading(false);
            setIsLoadingMore(false);
          }
        })
        .catch((error) => {
          {
            setError(error);
            setIsLoading(false);
          }
        });
    }
  };

  const handleLoadMore = () => {
    if (query == "") {
      console.log("in handlemore");
      let updatedVal = pageNo + 1;

      setIsLoadingMore(true);
      fetchRecords(updatedVal);
    }
  };

  const handleSearch = (text) => {
    const formattedQuery = text.toString().toLowerCase();
    setQuery(text);

    const filteredData = fullData.filter((user) => {
      return contains(user, formattedQuery);
    });
    setData(filteredData);
  };

  const contains = ({ name, species, status }, query) => {
    if (
      name.toString().toLowerCase().indexOf(query) != -1 ||
      species.toString().toLowerCase().indexOf(query) != -1 ||
      status.toString().toLowerCase().indexOf(query) != -1
    ) {
      return true;
    }

    return false;
  };
  const handleNavigation = (routeName, url, name) => {
    navigation.navigate(routeName, { url, name });
  };

  const renderHeader = () => {
    return (
      <View style={styles.searchBox}>
        <TextInput
          testID="inputText"
          autoCapitalize="none"
          autoCorrect={false}
          clearButtonMode="always"
          value={query}
          onChangeText={(queryText) => {
            handleSearch(queryText);
          }}
          placeholder="Search"
        />
      </View>
    );
  };
  const renderFooter = () => {
    if (!isLoadingMore) return null;

    return (
      <View style={styles.footerLoader}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  const renderListEmptyComponent = () => {
    if (data.length > 0) return null;

    return (
      <View style={styles.container}>
        <Text style={styles.noDataFound}>No Data Found :/</Text>
      </View>
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      {isLoading ? (
        <View style={styles.container}>
          <ActivityIndicator animating size="large" color={colors.white} />
        </View>
      ) : error ? (
        <View style={styles.container}>
          <Text style={{ fontSize: fonts.Large }}>
            Error fetching data... Check your network connection!
          </Text>
        </View>
      ) : (
        <View style={styles.container}>
          <FlatList
            testID="list"
            data={data}
            ListHeaderComponent={renderHeader}
            ListFooterComponent={renderFooter}
            ListEmptyComponent={renderListEmptyComponent}
            numColumns={1}
            keyExtractor={(item, i) => item.id.toString() + i}
            renderItem={({ item }) => (
              <CharacterCard {...item} handleNavigation={handleNavigation} />
            )}
            onEndReached={handleLoadMore}
            onEndReachedThreshold={0}
            initialNumToRender={20}
          />
        </View>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingVertical: 10,
    backgroundColor: colors.backgroundColor,
    justifyContent: "center",
  },
  searchBox: {
    backgroundColor: colors.mainThemeColor,
    padding: 10,
    margin: 10,
    borderRadius: 20,
  },
  footerLoader: {
    position: "relative",
    paddingVertical: 20,
    borderTopWidth: 1,
    marginTop: 10,
    marginBottom: 10,
  },
  noDataFound: {
    color: colors.white,
    fontSize: fonts.medium,
  },
});
