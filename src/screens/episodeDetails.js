import React, { useEffect, useState } from "react";
import {
  ActivityIndicator,
  Text,
  FlatList,
  SafeAreaView,
  View,
  StyleSheet,
} from "react-native";
import { EpisodeCard } from "../components/episodeCard";
import { colors, fonts } from "../utils/theme";
export const EpisodeDetails = ({ route }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);

  var _isMounted = false;

  useEffect(() => {
    _isMounted = true;
    fetchRecords();
    return () => {
      _isMounted = false;
    };
  }, []);

  const fetchRecords = () => {
    if (_isMounted) {
      Promise.all(
        route.params.url.map((url) => fetch(url).then((resp) => resp.json()))
      )
        .then((data) => {
          setData(data);
          setIsLoading(false);
        })
        .catch((error) => {
          setError(error);
          setIsLoading(false);
        });
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      {isLoading ? (
        <View style={styles.container}>
          <ActivityIndicator size="large" color={colors.white} />
        </View>
      ) : error ? (
        <View style={styles.container}>
          <Text style={{ fontSize: fonts.Large }}>
            Error fetching data... Check your network connection!
          </Text>
        </View>
      ) : (
        <FlatList
          testID="episode-list"
          data={data}
          keyExtractor={(item, i) => item.id.toString() + i}
          renderItem={({ item }) => <EpisodeCard data={item} />}
        />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingVertical: 10,
    backgroundColor: colors.backgroundColor,
  },
});
