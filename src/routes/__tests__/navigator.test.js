import "react-native";
import React from "react";

import { fireEvent, render, waitFor } from "react-native-testing-library";

import Navigator from "../navigator";

describe("Testing react navigation", () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });
  test("Home page Loading as Default Route contains one card", async () => {
    fetchMock.mockResponseOnce(
      JSON.stringify({
        results: [
          {
            created: "2017-11-04T18:48:46.250Z",
            episode: [
              "https://rickandmortyapi.com/api/episode/1",
              "https://rickandmortyapi.com/api/episode/2",
            ],
            gender: "Male",
            id: 1,
            image: "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
            location: {
              name: "Earth (Replacement Dimension)",
              url: "https://rickandmortyapi.com/api/location/20",
            },
            name: "Rick Sanchez",
            origin: {
              name: "Earth (C-137)",
              url: "https://rickandmortyapi.com/api/location/1",
            },
            species: "Human",
            status: "Alive",
            type: "",
            url: "https://rickandmortyapi.com/api/character/1",
          },
        ],
      })
    );
    const component = <Navigator />;
    const { getByTestId } = render(component);

    const listNode = await waitFor(() => getByTestId("list"));
    expect(listNode.children).toHaveLength(1);
    expect(fetchMock.mock.calls.length).toEqual(1);
    const characterName = await waitFor(() => getByTestId("name"));
    expect(characterName.props.children).toEqual("Rick Sanchez");
  });

  test("clicking on location takes you to the location details screen", async () => {
    fetchMock.mockResponse(async (req) => {
      if (req.url.match("https://rickandmortyapi.com/api/location/")) {
        return JSON.stringify({
          name: "Earth",
          type: "Planet",
          dimension: "s123",
          created: "11-01-2020",
        });
      } else {
        return JSON.stringify({
          results: [
            {
              created: "2017-11-04T18:48:46.250Z",
              episode: [
                "https://rickandmortyapi.com/api/episode/1",
                "https://rickandmortyapi.com/api/episode/2",
              ],
              gender: "Male",
              id: 1,
              image: "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
              location: {
                name: "Earth (Replacement Dimension)",
                url: "https://rickandmortyapi.com/api/location/20",
              },
              name: "Rick Sanchez",
              origin: {
                name: "Earth (C-137)",
                url: "https://rickandmortyapi.com/api/location/1",
              },
              species: "Human",
              status: "Alive",
              type: "",
              url: "https://rickandmortyapi.com/api/character/1",
            },
          ],
        });
      }
    });

    const component = <Navigator />;
    const { getByTestId, findByText } = render(component);
    const locationText = await waitFor(() => getByTestId("location"));
    fireEvent.press(locationText);
    const newHeading = await findByText("Location Dimension:");
    expect(newHeading).toBeTruthy();
    const locationdimension = await waitFor(() => getByTestId("dimension"));
    expect(locationdimension.props.children).toEqual("s123");
  });
});
