import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Home } from "../screens/home";
import { EpisodeDetails } from "../screens/episodeDetails";
import { LocationDetails } from "../screens/locationDetails";
import { colors, fonts } from "../utils/theme";

const AppStack = createStackNavigator();
export default function Navigator() {
  return (
    <NavigationContainer>
      <AppStack.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerStyle: {
            backgroundColor: colors.mainThemeColor,
            borderBottomWidth: 0,
          },
          headerTintColor: colors.white,
          headerTitleStyle: {
            fontWeight: "bold",
            fontSize: fonts.medium,
          },
        }}
      >
        <AppStack.Screen name="Home" component={Home} />
        <AppStack.Screen
          name="Episode"
          component={EpisodeDetails}
          options={({ route }) => ({
            title: route.params["name"] + "-" + "Episodes",
          })}
        />
        <AppStack.Screen
          name="Location"
          component={LocationDetails}
          options={({ route }) => ({
            title: route.params["name"] + "-" + "Location",
          })}
        />
      </AppStack.Navigator>
    </NavigationContainer>
  );
}
